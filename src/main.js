const {Blockchain, Transaction} = require('./blockchain.js');
const EC = require('elliptic').ec;
const ec = new EC('secp256k1');

// you have to keep this private key only for yourself!!!
const myKey = ec.keyFromPrivate('cf81156809195768449c2756227b0e8e4b175b000267357766bb886405c518ef');
const myWalletAddress = myKey.getPublic('hex');


let mcCoin = new Blockchain();

const tx1 = new Transaction(myWalletAddress, 'public key goes here', 5);
tx1.signTransaction(myKey);
mcCoin.addTransaction(tx1);

console.log('\n Starting the miner...');
mcCoin.minePendingTransactions(myWalletAddress);

console.log('\n Balance of Maras is', mcCoin.getBalanceOfAddress(myWalletAddress));

// example of tampering into blockchain
mcCoin.chain[1].transactions[0].amount = 1;

console.log('Is chain valid?', mcCoin.isChainVaild());
